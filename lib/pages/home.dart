import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final int initNumber;

  const HomePage({this.initNumber, this.callback});

  final Function callback;

  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  int number = 0;
  @override
  void initState() {
    number = this.widget.initNumber;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Tesst Pluggin"),
      ),
      body: Center(
        child: InkWell(
          onTap: () {
            this.widget.callback(number + 1);
            setState(() {
              number = number + 1;
            });

            print(number);
          },
          child: Container(
            width: 50.0,
            height: 50.0,
            color: Colors.redAccent,
            child: Center(
                child: Text(
              '$number',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold),
            )),
          ),
        ),
      ),
    );
  }
}
