import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_plugin/pages/home.dart';

class FlutterPlugin {
  static const MethodChannel _channel = const MethodChannel('flutter_plugin');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String> get name async {
    return "Pham Trung Kieen";
  }

  static void countnumber(BuildContext context, {int initNumber, Function callback}) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => HomePage(
              initNumber: initNumber,
          callback: callback,
            )));
  }

  static Function callbackFunc(int number){
    // return number;
  }
}
